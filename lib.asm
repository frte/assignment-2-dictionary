global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_command
global parse_uint
global parse_int
global string_copy

section .text
%define EXIT_CODE 60 
%define STDIN 0
%define STDOUT 1
%define STDERR 2
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0x0
    jz .break
    inc rax
    jmp .loop
.break:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, STDOUT
    mov rdi, STDOUT
    syscall
    ret

print_error:
    xor rax, rax
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, STDOUT
    mov rdi, STDERR
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rbp
    mov rbp, rsp
    mov rax, STDOUT
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    mov rsp, rbp
    pop rbp
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char
   

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbp
    mov rbp, rsp
    sub rsp, 32
    mov rax, rdi
    mov rcx, 10
    dec rsp
    mov byte [rsp], 0
    .loop:
        xor rdx, rdx
        dec rsp
        div rcx
        add dl, '0'
        mov byte [rsp], dl
        test rax, rax
        jne .loop
    mov rdi, rsp
    call print_string
    mov rsp, rbp
    pop rbp
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0x0
    jge .positive
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .positive:
	jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .loop:
	mov al, byte[rdi]
	mov cl, byte[rsi]
	cmp al, cl
	jne .not_equals
	cmp al, 0	
	je .equals
	inc rdi
	inc rsi
	jmp .loop
    .equals:
	mov rax, STDOUT
	ret
    .not_equals:
        xor rax, rax
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, STDIN
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .end_of_stream
    mov al, [rsp]
    .end_of_stream:
	pop rdi
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rdi
    push rsi
    push rcx
    mov rdi, rdi
    mov rsi, rsi
    mov rcx, rcx
    xor rcx, rcx
    .next_char:
        call read_char
        cmp rax, ' '
        jz .next_char
        cmp rax, `\t`
        jz .next_char
        cmp rax, `\n`
        jz .next_char
        jmp .no_chars


    .loop:
        call read_char
        cmp rax, ' '
        jz .success
        cmp rax, '\t'
        jz .success
        cmp rax, '\n'
        jz .success
        .no_chars:
            test rax, rax
            jz .success
            mov byte[rdi+rcx], al
            inc rcx
            cmp rcx, rsi
            jna .loop
    xor rax, rax
    jmp .out


    .success:
        test rcx, rcx
        jz .break
        mov byte[rdi+rcx], 0
        .break:
            mov rax, rdi
            mov rdx, rcx
            .out:
                pop rcx
                pop rsi
                pop rdi
                ret  
	
read_command:
    push rdi
    push rsi
    push rcx
    mov rdi, rdi
    mov rsi, rsi
    mov rcx, rcx
    xor rcx, rcx
    .next_char:
        call read_char
        cmp rax, `\n`
        jz .next_char
        jmp .no_chars


    .loop:
        call read_char
        cmp rax, '\n'
        jz .success
        .no_chars:
            test rax, rax
            jz .success
            mov byte[rdi+rcx], al
            inc rcx
            cmp rcx, rsi
            jna .loop
    xor rax, rax
    jmp .out


    .success:
        test rcx, rcx
        jz .break
        mov byte[rdi+rcx], 0
        .break:
            mov rax, rdi
            mov rdx, rcx
            .out:
                pop rcx
                pop rsi
                pop rdi
                ret
   
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    mov r8, 10

    .loop:
        movzx rcx, byte [rdi + rdx]
        test rcx, rcx
        jz .break
        cmp rcx, '0' 
        jl .break
        cmp rcx, '9'  
        jg .break
        push rdx
        mul r8
        pop rdx
        sub rcx, '0'
        add al, cl
        inc rdx
        jmp .loop
    .break:
    	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '+'
    je .positive_with_plus
    cmp byte [rdi], '-'
    jne .positive
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .zero_length
    neg rax 
    inc rdx
    ret
    .positive:
	call parse_uint
	ret
    .positive_with_plus:
	inc rdi
	call parse_uint
	test rdx, rdx
	jne .zero_length
	inc rdx
	ret
    .zero_length:
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jnb .too_long
    push rax
    inc rax
    .loop:
	mov cl, [rdi]
	mov [rsi], cl
	inc rsi
	inc rdi
	dec rax
	test rax, rax
	jne .loop
    pop rax
    ret
    .too_long:
	xor rax, rax
	ret
	
