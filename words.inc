%include "colon.inc"

section .rodata

colon "third word with space and symbols +-*/.", third_word
db "third word with space and symbols +-*/.", 0

colon "second word with space", second_word
db "second word with space", 0 

colon "first word", first_word
db "first", 0 
