import subprocess
make = "make build"
main = "./main"

label_list = [["first word", "first", ""], ["second word with space", "second word with space", ""], ["third word with space and symbols +-*/.", "third word with space and symbols +-*/.", ""], ["The key is not exist", "", "Error: The key is not exist!"], [" ", "", "Error: The key is not exist!"], ["", "", "Error: The key is not exist!"], ["TooLoooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooongWord", "", "Error: The key is too long!"], ["NotTooLoooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooongWord", "Not too long word", ""]]

def test(label, label_stdout, label_stderr):
	process = subprocess.Popen([main], stdin=subprocess.PIPE, stdout=subprocess.PIPE, 		stderr=subprocess.PIPE)	
	stdout, stderr = process.communicate(input=label)
	print("stdin: " + label)
	print("stdout: " + stdout.decode().strip())
	print("stderr: " + stderr.decode().strip())
	if stdout != label_stdout:
		print("Error in stdout, got: '" + stdout + "', but expected: '" + 			label_stdout + "'") 
		return True
	if stderr != label_stderr:
		print("Error in stderr, get '" + stderr + "', but expected: '" + 			label_stderr + "'")
		return True	
	else:
		print("Process is correct!")
		return False

output = subprocess.check_output(make, shell=True)
print(output)

count = 0
print("Start testing...")
for i in range(len(label_list)):
	print("----------------------------------------")
	if test(label_list[i][0], label_list[i][1], label_list[i][2]):
		count += 1
print("----------------------------------------")
if count != 0:
	print("Count of errors in tests: " + str(count))
else:
	print("All tests are correct!")

