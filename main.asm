%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define MAX_SIZE 256

section .bss
    buff: resb MAX_SIZE
section .rodata
    error_lenght: db 'Error: The key is too long!', 0 
    error_exist: db 'Error: The key is not exist!', 0
section .text
global _start
_start:
    mov rdi, buff
    mov rsi, 255
    call read_command
    test rax, rax
    je .err_lenght

    mov rdi, rax
    mov rsi, element
    call find_word
    test rax, rax
    je .err_exist

    add rax, 8
    mov rdi, rax
    push rdi 
    call string_length
    pop rdi
    inc rdi
    add rdi, rax
    call print_string
    jmp .break
    .err_lenght:
	mov rdi, error_lenght
	call print_error
 	jmp .break
    
    .err_exist:
 	mov rdi, error_exist
 	call print_error
 	jmp .break	
    .break:
	xor rdi, rdi
	jmp exit
