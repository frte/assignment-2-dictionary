%include "lib.inc"

global find_word

section .text

find_word:
    push rdx
    push rsi
    xor rax, rax
    .loop:
        test rsi, rsi
        jz .break
        mov rbx, rsi
        lea rsi, [rsi + 8] 
        call string_equals
        test rax, rax
        jne .success
        mov rsi, [rbx]
        jmp .loop
    .success:
        mov rax, rbx
    .break:
        pop rsi
        pop rbx
        ret
